" Some basic settings
set number
set encoding=utf-8
syntax enable
set noswapfile
set scrolloff=7
set backspace=indent,eol,start
let mapleader = ' '
let NERDTreeShowHidden=1

" Python specific settings
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
set autoindent

" Plugin manager Plug
call plug#begin('~/.vim/plugged')

Plug 'jiangmiao/auto-pairs'
Plug 'scrooloose/nerdtree'
Plug 'norcalli/nvim-colorizer.lua'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

call plug#end()

if (has("termguicolors"))
    set termguicolors
endif

lua require 'colorizer'.setup()


" Keybinds
nnoremap <C-x> :NERDTree<CR>
nnoremap <C-n> :bnext<CR>
nnoremap <C-b> :bprev<CR>

" Tabs
let g:airline#extensions#tabline#enabled=1
let g:airline#extensions#tabline#fnamemode=':t'
nmap <leader>1 :bp<CR>
nmap <leader>2 :bn<CR>
nmap <C-w> :bd<CR>

